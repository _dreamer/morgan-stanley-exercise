#!/usr/bin/python3

def calc_area(*args):
    '''This function finds total area covered 
        by a given list of line coordinates. 
        EXPERIMENTAL VERSION!!'''

    coordinates = sorted(list(args))

    area = 0
    start, end = (0, 0), (0, 0)
    s = {}
    for i in range(len(coordinates) - 1):
        if coordinates[i+1][0] <= coordinates[i][1]:
            if coordinates[i][0] != end[0] and coordinates[i][1] != end[1]:
                start = coordinates[i]
            end = coordinates[i+1]
            for j in gen(coordinates[i+2:]):
                try:
                    if end[0] <= j[1]:
                        end = j
                    else:
                except IndexError:
                    break
            area += end[1] - start[0]
        else:
            area += coordinates[i][1] - coordinates[i][0] 
    print(area)

    outputStr = 'Total Area Covered By {} is {}'.format(coordinates, area)
    print(outputStr)

def gen(*args):
    for i in sorted(list(args)):
        yield i

if __name__ == '__main__':
    data = [(0, 2), (5, 7), (6, 9)]
    #data = [(0, 2), (4, 6), (5, 7), (6, 9)]
    #data = [(0, 2), (5, 7), (6, 9), (9, 10), (11, 13)]
    calc_area(*data)