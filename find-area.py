#!/usr/bin/python3

def calc_area(*args):
    '''This function finds total area covered 
        by a given list of line coordinates.'''

    coordinates = sorted(list(args))

    if len(coordinates) <= 3:
        lengths = [coordinates[i+1][1] - coordinates[i][0] 
                    if coordinates[i+1][0] <= coordinates[i][1] 
                    else coordinates[i][1] - coordinates[i][0] 
                    for i in range(len(coordinates) - 1)]
        area = sum(lengths)
    else:
        area = 0
        overlaps = 0
        for i in range(len(coordinates) - 1):
            if coordinates[i+1][0] <= coordinates[i][1]:
                area += coordinates[i+1][1] - coordinates[i][0]
                if coordinates[i+1][0] - coordinates[i][1] == 0:
                    overlaps += 1
                else:
                    overlaps += abs(coordinates[i+1][0] - coordinates[i][1])
            else:
                area += coordinates[i][1] - coordinates[i][0] 
        area = area - overlaps

    outputStr = 'Total Area Covered By {} is {}'.format(coordinates, area)
    print(outputStr)

if __name__ == '__main__':
    #data = [(0, 2), (5, 7), (6, 9)]
    data = [(0, 2), (4, 6), (5, 7), (6, 9)]
    #data = [(0, 2), (5, 7), (6, 9), (9, 10), (11, 13)]
    calc_area(*data)
    
    